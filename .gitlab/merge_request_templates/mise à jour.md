# Mise à jour d'un paramètre législatif

## Champ de la mise à jour 

(Merci d'effacer les blocs inutiles)

- chomage
/label chomage
/assign @benjello  	

- impot-revenu 
/label impot-revenu
/assign @ClaireLeroyIPP 

- marche-travail 
/label marche-travail
/assign @audreyrain	

- prelevements-sociaux 
/label prelevements-sociaux
/assign @SophieIPP

- prestations-sociales
/label prestations-sociales
/assign @MarionIPP

- retraite 
/label retraite
/assign @audreyrain

- tarifs-energie 
/label tarifs-energie
/assign @thomas.douenne

- taxation-capital 
/label taxation-capital
/assign @bfabre

- taxation-indirecte 
/label taxation-indirecte
/assign @benjello

## Description de la mise à jour

Décrire brièvement le mise à jour réalisée

----

## Quelques conseils à prendre en compte

### Vérifications à effectuer par le contributeur

- [ ] Décrire la mise à jour et son champ
- [ ] Renseigner la référence législative
- [ ] Renseigner la date de parution au JO


### Vérifications à effectuer par le relecteur

- [ ] Vérifier que la date de parution au JO est renseignée et plausible
- [ ] Vérifier que la référence législative est renseignée et plausible
- Ne pas oublier de vérifier que le site web est fonctionnel en cliquant [ici]((https://french-tax-and-benefit-tables.frama.io/baremes-ipp-yaml/) et en complétant l'URL par le nom de la branche.

