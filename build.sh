#!/bin/bash

set -ex

cd .openfisca
pip install --editable .
pip install 'openfisca_core[web-api]<35'
openfisca serve -p 2000 &
wget --retry-connrefused --waitretry=1 --quiet http://localhost:2000
cd ..
{
    git clone https://framagit.org/french-tax-and-benefit-tables/baremes-ipp-views.git
    cd baremes-ipp-views
    git fetch
    git pull
    echo "TABLES_DIR=`pwd`/../tables" >> .env
    [ "$1" = "--prod" ] && cat .env-prod >> .env
    cat .env
    npm install && npm run  build && npm run export
    mv out ../dist
    cd ..
} || {
    kill $(jobs -p)
    exit 1
}
kill $(jobs -p)
