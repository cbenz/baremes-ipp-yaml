# Déploiement sur ipp.eu

Les mises à jour de la branche `master` de ce dépôt sont régulièrement et automatiquement déployées sur https://www.ipp.eu/baremes-ipp/.

Un fichier ZIP contenant les pages partielles à intégrer sur le site est publié sur la branche `ipp.eu-wp-zip` par GitLab CI (voir [configuration](../.gitlab-ci.yml)) à chaque mise à jour de `master`.

Le traitement de ce fichier par WordPress est décrite dans une [documentation spécifique](https://bitbucket.org/juliabr/ipp.eu/src/master/wp-content/themes/ipp/README_BAREMES.md) sur le dépôt du site.
